for (let num=1;num<=100;num++){
    if(num%3==0&&num%5==0){
        console.log(`FizzBuzz (${num})`) // Multiple of 3 and 5 "FizzBizz"
    }else if(num%3==0){
        console.log(`Fizz (${num})`) // Multiple of 3 "Fizz"
    }else if(num%5==0){
        console.log(`Buzz (${num})`) // Multiple of 5 "Buzz"
    }
    else{
        console.log(num)
    }
}